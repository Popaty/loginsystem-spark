package com.skoode.login.services;

import com.skoode.login.model.SessionLogin;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 10/20/2015 AD.
 */
public class SessionLoginServiceTest {

    private SessionLoginService sessionLoginService;
    @org.junit.Before
    public void setUp() throws Exception {
        sessionLoginService = new SessionLoginService();
    }

    @org.junit.After
    public void tearDown() throws Exception {
        sessionLoginService = null;
    }

    @org.junit.Test
    public void testInsertSessionLogin() throws Exception {
        SessionLogin sessionLogin = new SessionLogin();
        sessionLogin.setSessionName("e08ck9pi7yjy1cxc5r731oeez@163621948");

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
        Date date = new Date();

        System.out.println(date);

        sessionLogin.setExpiredDate(date);
        sessionLogin.setUserName("popaty");
        sessionLoginService.insertSessionLogin(sessionLogin);
        System.out.println(sessionLogin);
    }

    @Test
    public void testGetSessionLoginByName() throws Exception {
        SessionLogin sessionLogin = sessionLoginService.getSessionLoginByName("e08ck9pi7yjy1cxc5r731oeez@163621948");
        System.out.println(sessionLogin);
    }
}