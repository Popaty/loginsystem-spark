package com.skoode.login.services;

import com.skoode.login.api.v1.LoginController;
import com.skoode.login.model.Member;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 10/20/2015 AD.
 */
public class MemberServicesTest {

    private MemberServices memberServices;
    private LoginController loginController;
    @Before
    public void setUp() throws Exception {
        memberServices = new MemberServices();
        loginController = new LoginController();
    }

    @After
    public void tearDown() throws Exception {
        memberServices = null;
        loginController = null;
    }

    @Test
    public void testCheckMemberPassword() throws Exception {
        Member member = memberServices.checkMemberPassword("petch");
        String mdPAssword = loginController.encryptPassword("password1");

        if (mdPAssword.equals(member.getPassword())){
            System.out.println("Right password");
        }else{
            System.out.println("Wrong PAssword");
        }
        System.out.println(member);

    }
}