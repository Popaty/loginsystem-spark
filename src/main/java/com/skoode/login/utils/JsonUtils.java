package com.skoode.login.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class JsonUtils {

    public static String toJson(Object obj) throws JsonProcessingException {

        ObjectMapper om = new ObjectMapper();
        om.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm a z"));
        ObjectWriter writer = om.writer().withDefaultPrettyPrinter();
        return writer.writeValueAsString(obj);
    }

    public static<T> T asObject(String json, Class<T> cls) throws IOException {
        ObjectMapper om = new ObjectMapper();
        om.setDateFormat(new SimpleDateFormat("yyy-MM-dd HH:mm a z"));
        ObjectReader reader = om.reader().forType(cls);
        return reader.readValue(json);
    }
}
