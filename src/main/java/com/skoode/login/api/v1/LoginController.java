package com.skoode.login.api.v1;

import com.skoode.login.model.Member;
import com.skoode.login.model.SessionLogin;
import com.skoode.login.services.MemberServices;
import com.skoode.login.services.SessionLoginService;
import com.skoode.login.utils.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import spark.Session;
import spark.Spark;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;



/**
 * Created by Maximiliano on 10/16/2015 AD.
 */
public class LoginController {


    private boolean result;
    private String message;
    private Map map = new HashMap();
    private MemberServices memberServices = new MemberServices();
    private SessionLoginService sessionLoginService = new SessionLoginService();
    private Date expireDate;


    public void mapRoute(){

        Spark.get("/api/login",(request, response) -> {
            response.type("application/json");
            String result = "true";
            Map map = new HashMap();
            map.put("message","true");
            return JsonUtils.toJson(map);
        });


        Spark.post("/api/login", (request, response) -> {
            String username = request.queryMap().get("username").value();
            String password = request.queryMap().get("password").value();
            String token = request.queryMap().get("token").value();



            System.out.println(username);
            System.out.println(password);
            System.out.println(token);

            Session session;
            session = request.session(true);
            String sessionraw = session.raw().toString();
//            System.out.println("sessionraw :"+sessionraw);
            String jsesesionid = StringUtils.substringAfter(sessionraw,":");


            if(username == null || password == null){
                map = new HashMap();
                map.put("message",false);
            }else{

                boolean result = checkPassword(username,password);
                if(result){
                    if(token == null){
                        SessionLogin sessionLogin = new SessionLogin();
                        sessionLogin.setUserName(username);

                        Calendar c = Calendar.getInstance();
                        c.setTime(new Date());
                        c.add(Calendar.DATE, 1);
                        expireDate = c.getTime();
//                        System.out.println(newDate);
                        sessionLogin.setExpiredDate(expireDate);
                        sessionLogin.setSessionName(jsesesionid);
                        sessionLoginService.insertSessionLogin(sessionLogin);
                    }else{
                        jsesesionid = token;
//                        System.out.println(token);
//                        System.out.println(jsesesionid);
                        map = checkSession(username,jsesesionid);
                        response.type("application/json");
                        return JsonUtils.toJson(map);
                    }


                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
                    Date date = new Date();
//                System.out.println(dateFormat.format(date));

                    map = new HashMap();
                    map.put("message",true);
                    map.put("timestamp",dateFormat.format(date));
                    map.put("expire_date",expireDate);
                    map.put("token",jsesesionid);
                }else {
                    map = new HashMap();
                    map.put("result",result);
                    map.put("message","your password is wrong!!!");
                }

            }
            response.type("application/json");
            return JsonUtils.toJson(map);
        });

        Spark.post("api/checksession", (request, response) -> {
//            String username = request.queryMap().get("username").value();
            String username = request.queryMap().get("username").value();
            String tokenID = request.queryMap().get("token").value();
//            System.out.println(username);
//            System.out.println(tokenID);
            map = checkSession(username, tokenID);
            response.type("application/json");
            return JsonUtils.toJson(map);
        });
    }

    private boolean checkPassword(String username, String password) throws Exception {
        boolean result = false;

        Member member = memberServices.checkMemberPassword(username);
        String mdPAssword = encryptPassword(password);

        if (mdPAssword.equals(member.getPassword())){
            result = true;
        }else{
            result = false;
        }
        return result;
    }

    private Map checkSession(String username, String tokenID) {
        SessionLogin sessionLogin = sessionLoginService.getSessionLoginByName(tokenID.trim());
        if(sessionLogin==null){
            result = false;
            message = "your session is exceed limit, please try to login again!";

            map = new HashMap();

            map.put("result",result);
            map.put("message",message);
            return map;
        }
//        System.out.println(sessionLogin.toString());

//            System.out.println((sessionLogin.getExpiredDate().getTime()/1000)/60);
        Date currentDate = new Date();
//            System.out.println((currentDate.getTime() / 1000) / 60);
        long expireTime = ((sessionLogin.getExpiredDate().getTime()/1000)/60);
        long currentTime = ((currentDate.getTime()/1000)/60);


        if(currentTime - expireTime > 1440){
            result = false;
            message = "your session is exceed limit, please try to login again!";
        }
        else{
            result = true;
            message = "your session is going, please continue your work!";
        }
        map = new HashMap();

        map.put("result",result);
        map.put("message",message);
        return map;
    }

    public String encryptPassword(String string) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] stByte = string.getBytes();
        byte[] mdByte = md.digest(stByte);

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mdByte.length; i++) {
            sb.append(Integer.toString((mdByte[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }


}
