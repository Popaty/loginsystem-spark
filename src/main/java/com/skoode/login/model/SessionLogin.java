package com.skoode.login.model;

import java.util.Date;

/**
 * Created by Maximiliano on 10/20/2015 AD.
 */
public class SessionLogin {
    private int id;
    private String sessionName;
    private Date expiredDate;
    private String userName;

    @Override
    public String toString() {
        return "SessionLogin{" +
                "id=" + id +
                ", sessionName='" + sessionName + '\'' +
                ", expiredDate=" + expiredDate +
                ", userName='" + userName + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
