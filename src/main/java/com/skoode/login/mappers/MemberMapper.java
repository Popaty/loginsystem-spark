package com.skoode.login.mappers;

import com.skoode.login.model.Member;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 * Created by Maximiliano on 10/20/2015 AD.
 */
public interface MemberMapper {

    @Select("select * from member where username = #{param1}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "username", column = "username"),
            @Result(property = "password", column = "password"),
    })
    public Member checkMemberPassword(String username);
}
