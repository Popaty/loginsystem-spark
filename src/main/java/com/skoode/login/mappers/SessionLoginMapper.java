package com.skoode.login.mappers;

import com.skoode.login.model.SessionLogin;
import org.apache.ibatis.annotations.*;

/**
 * Created by Maximiliano on 10/20/2015 AD.
 */
public interface SessionLoginMapper {

    @Insert("insert into session (session_name,expired_date,username)" +
            "values (#{sessionName},#{expiredDate},#{userName})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public int insertSessionLogin(SessionLogin sessionLogin);


    @Select("select * from session where session_name = #{param1}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "sessionName", column = "session_name"),
            @Result(property = "expiredDate", column = "expired_date"),
            @Result(property = "userName", column = "username"),
    })
    public SessionLogin getSessionLoginByName(String sessionName);

}
