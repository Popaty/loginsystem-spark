package com.skoode.login.services;

import com.skoode.login.mappers.MemberMapper;
import com.skoode.login.model.Member;
import org.apache.ibatis.session.SqlSession;

/**
 * Created by Maximiliano on 10/20/2015 AD.
 */
public class MemberServices implements MemberMapper{

    @Override
    public Member checkMemberPassword(String username) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            MemberMapper memberMapper = sqlSession.getMapper(MemberMapper.class);
            return memberMapper.checkMemberPassword(username);
        } finally {
            sqlSession.close();
        }
    }
}
