package com.skoode.login.services;

import com.skoode.login.mappers.SessionLoginMapper;
import com.skoode.login.model.SessionLogin;
import org.apache.ibatis.session.SqlSession;

/**
 * Created by Maximiliano on 10/20/2015 AD.
 */
public class SessionLoginService implements SessionLoginMapper{

    @Override
    public int insertSessionLogin(SessionLogin sessionLogin) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            SessionLoginMapper sessionloginMapper = sqlSession.getMapper(SessionLoginMapper.class);
            int effectrow = sessionloginMapper.insertSessionLogin(sessionLogin);
            sqlSession.commit();
            return effectrow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public SessionLogin getSessionLoginByName(String sessionName) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            SessionLoginMapper sessionloginMapper = sqlSession.getMapper(SessionLoginMapper.class);
            return sessionloginMapper.getSessionLoginByName(sessionName);
        } finally {
            sqlSession.close();
        }
    }
}
